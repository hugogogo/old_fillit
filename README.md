# Fillit

Team: **hulamy** and **vmanzoni**

Le but de ce projet est d’agencer les Tetriminos entre eux pour former le plus petit carré possible, sachant que ce carré peut présenter des trous quand les pièces données ne s’emboîtent pas parfaitement.

## To do

- [x] Check if we have a file
- [x] Read file
- [x] Check if there are errors in file
- [x] Check if every tetrimino is valid
- [x] Transform file into tetriminos
- [x] Backtracking for smallest square
- [x] Transform tetriminos to letters
- [x] Print result (Can be done with above step)
- [ ] Free everything (NO LEAKS)

## BONUS
- [x] Best error handler (more details on why there is an error.)
- [x] Optimisation (skip when tetri with same shape was already tested on map)
- [x] Add colors to tetri when printing result map
- [x] Flag for debbuging (print every step in backtracking)
- [x] Adding flags (and password for blocking more args on moulinette)
